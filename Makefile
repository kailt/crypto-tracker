BINARY_NAME=crypto-tracker

GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get -u

all: test build
build:
	$(GOBUILD) -o $(BINARY_NAME) -v
test: 
	$(GOTEST) -v ./...
run:
	$(GOBUILD) -o $(BINARY_NAME) -v
	./$(BINARY_NAME)
clean: 
	$(GOCLEAN)
	rm -f $(BINARY_NAME)
deps:
	$(GOGET) github.com/facebookgo/grace/gracehttp
	$(GOGET) github.com/spf13/viper
	$(GOGET) github.com/gorilla/mux
	$(GOGET) github.com/rcrowley/go-metrics
	$(GOGET) github.com/rcrowley/go-metrics/exp
	$(GOGET) bitbucket.org/kailt/crypto-tracker/server
	$(GOGET) bitbucket.org/kailt/crypto-tracker/collections
