package server

import (
	"log"
	"net/http"

	"bitbucket.org/kailt/crypto-tracker/collections"

	"github.com/gorilla/mux"
	metrics "github.com/rcrowley/go-metrics"
)

// API routes
func (s *Server) routes() {
	s.Handle("/debug/metrics", http.DefaultServeMux) // apm metrics
	s.HandleFunc("/products", s.handleProducts())
	s.HandleFunc("/products/{product}/prices", s.handleProductPrices())
}

var exchanges = [...]string{"BTX", "BNB", "BFX"}

type product struct {
	ID string `json:"id"`
}

func (s *Server) handleProducts() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		t := metrics.GetOrRegisterTimer("api.products.latency", nil)
		t.Time(func() {
			respond(w, r, http.StatusOK, s.commonProducts().List())
		})
	}
}

func (s *Server) handleProductPrices() http.HandlerFunc {
	type productPrice struct {
		Price    float64 `json:"price"`
		Exchange string  `json:"exchange"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		t := metrics.GetOrRegisterTimer("api.product.prices.latency", nil)
		t.Time(func() {
			vars := mux.Vars(r)

			commonProducts := s.commonProducts()

			if !commonProducts.Contains(vars["product"]) {
				respondHTTPErr(w, r, http.StatusNotFound)
				return
			}

			var prices []productPrice

			channel := make(chan productPrice, len(exchanges))

			for _, exch := range exchanges {
				// Do it in parallel
				go func(exchange string) {
					// Get price
					req, _ := http.NewRequest("GET", s.apiEndpoint+"/exchanges/"+exchange+"/ticker", nil)
					q := req.URL.Query()
					q.Add("product", vars["product"])
					req.URL.RawQuery = q.Encode()
					req.Header.Add("Authorization", "Bearer "+s.apiKey)
					resp, err := s.apiClient.Do(req)
					if err != nil {
						log.Println(err)
						return
					}

					// Deserialize JSON
					var price productPrice
					err = DecodeBody(resp.Body, &price)
					if err != nil {
						log.Println(err)
						return
					}
					price.Exchange = exchange

					channel <- price
				}(exch)
			}

			for i := 0; i < len(exchanges); i++ {
				prices = append(prices, <-channel)
			}

			respond(w, r, http.StatusOK, prices)
		})
	}
}

func (s *Server) commonProducts() *collections.Set {
	commonProducts := collections.NewSet()
	channel := make(chan *collections.Set, len(exchanges))

	for _, exch := range exchanges {
		// Do it in parallel
		go func(exchange string) {
			// Get products
			req, _ := http.NewRequest("GET", s.apiEndpoint+"/exchanges/"+exchange+"/products", nil)
			req.Header.Add("Authorization", "Bearer "+s.apiKey)
			resp, err := s.apiClient.Do(req)
			if err != nil {
				log.Println(err)
				return
			}

			// Deserialize JSON
			var products []product
			err = DecodeBody(resp.Body, &products)
			if err != nil {
				log.Println(err)
				return
			}
			productSet := collections.NewSet()
			for _, p := range products {
				productSet.Add(p.ID)
			}
			channel <- productSet
		}(exch)
	}

	for i := 0; i < len(exchanges); i++ {
		p := <-channel
		// Set intersection to find common products across exchanges
		if commonProducts.Size() == 0 {
			commonProducts = p
		} else {
			commonProducts = commonProducts.Intersect(p)
		}
	}

	return commonProducts
}
