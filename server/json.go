package server

import (
	"encoding/json"
	"io"
	"net/http"
)

// DecodeBody decodes JSON response body to a Go value
func DecodeBody(body io.ReadCloser, v interface{}) error {
	defer body.Close()
	return json.NewDecoder(body).Decode(v)
}

// EncodeBody encodes a Go value to JSON and writes it to an http.ResponseWriter
func EncodeBody(w http.ResponseWriter, v interface{}) error {
	return json.NewEncoder(w).Encode(v)
}
