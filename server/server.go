package server

import (
	"net/http"
	"time"

	"github.com/gorilla/mux"
	metrics "github.com/rcrowley/go-metrics"
	"github.com/rcrowley/go-metrics/exp"
)

func init() {
	exp.Exp(metrics.DefaultRegistry)
}

// Server type embeds a router and the necessary info to query the Moneeda api
type Server struct {
	apiEndpoint string
	apiKey      string
	apiClient   *http.Client
	*mux.Router
}

// New creates a new Server instance, its routes and returns it
func New(endpoint string, key string) *Server {
	s := &Server{
		apiEndpoint: endpoint,
		apiKey:      key,
		apiClient: &http.Client{
			Timeout: 10 * time.Second,
		},
		Router: mux.NewRouter().StrictSlash(true),
	}
	s.routes()
	return s
}
