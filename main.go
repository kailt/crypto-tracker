package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/kailt/crypto-tracker/server"

	"github.com/facebookgo/grace/gracehttp"
	"github.com/spf13/viper"
)

// PIDFile defines the path and file of a PID file
type PIDFile struct {
	Path string
	File *os.File
}

var pid PIDFile

func init() {
	// Global log config
	log.SetFlags(log.LstdFlags | log.Lmicroseconds | log.Lshortfile)

	// PID file
	pid.Path = "/tmp/" + path.Base(os.Args[0]) + ".pid"
	var err error
	pid.File, err = os.Create(pid.Path)
	if err != nil {
		log.Fatalln("Error creating pidfile: " + err.Error())
	}
	fmt.Fprint(pid.File, os.Getpid())
}

func main() {
	defer exit()

	// Read config
	viper.SetDefault("server.port", 8080)
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	viper.SetEnvPrefix("CT")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	// Graceful shutdown/reload
	// SIGINT/SIGSTOP for graceful shutdown
	// SIGUSR2 for graceful reload (zero-downtime deployment)
	// See https://godoc.org/github.com/facebookgo/grace/gracehttp
	gracehttp.SetLogger(log.New(os.Stderr, "GRACE: ", log.Ldate|log.Ltime))
	if err := gracehttp.Serve(&http.Server{
		Addr:         ":" + viper.GetString("server.port"),
		Handler:      server.New(viper.GetString("moneeda.endpoint"), viper.GetString("moneeda.key")),
		ReadTimeout:  15 * time.Second,
		WriteTimeout: 15 * time.Second,
	}); err != nil {
		panic(err)
	}
}

func exit() {
	var exitCode int
	if r := recover(); r != nil {
		log.Println(r)
		exitCode = 1
	} else {
		exitCode = 0
	}
	if pid.File != nil {
		pid.File.Close()
		if buf, _ := ioutil.ReadFile(pid.Path); string(buf) == strconv.Itoa(os.Getpid()) {
			if err := os.Remove(pid.Path); err != nil {
				log.Fatalln("Error removing pidfile: " + pid.Path)
			}
		}
	}
	os.Exit(exitCode)
}
