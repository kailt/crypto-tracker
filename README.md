# Crypto tracker

Crypto tracker API server built with Go.

## Build requirements

- A working Go installation. See: https://golang.org/doc/install
- `make` in order to use the [`Makefile`](./Makefile)

## Endpoints

- `/products`: returns the common crypto products of the "BTX", "BNB", "BFX" exchanges.
- `/products/{product}/prices`: get the prices of a given product from the aforementioned exchanges.
- `/debug/metrics`: internal instrumentation. Latency metrics, runtime metrics, etc.