package collections

// Set is a collection of unique values
type Set struct {
	vals map[string]struct{}
}

// NewSet returns a new Set
func NewSet() *Set {
	s := &Set{
		vals: make(map[string]struct{}),
	}
	return s
}

// Add adds a value to the Set
func (s *Set) Add(vals ...string) {
	for _, v := range vals {
		s.vals[v] = struct{}{}
	}
}

// Size returns the number of elements in the Set
func (s *Set) Size() int {
	return len(s.vals)
}

// List returns the values of the Set as a slice
func (s *Set) List() []string {
	keys := make([]string, len(s.vals))
	i := 0
	for k := range s.vals {
		keys[i] = k
		i++
	}
	return keys
}

// Contains returns true if the value is present in the Set
func (s *Set) Contains(vals ...string) bool {
	for _, v := range vals {
		_, ok := s.vals[v]
		if !ok {
			return false
		}
	}

	return true
}

// Intersect returns the intersection of the two Sets
func (s *Set) Intersect(s2 *Set) *Set {
	res := NewSet()
	for v := range s.vals {
		if !s2.Contains(v) {
			continue
		}
		res.Add(v)
	}
	return res
}
